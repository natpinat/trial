from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request,'blog/index.html',{})

def info(request):
    return render(request,'blog/info.html',{})